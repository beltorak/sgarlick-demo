package com.home.web.controller;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestOperations;

import com.home.web.domain.Users;

@Controller
@RequestMapping(value = "rest")
public class RestTestController {

	private static final Logger log = LoggerFactory
			.getLogger(RestTestController.class);

	@Resource
	private RestOperations restOperations;

	@RequestMapping(method = RequestMethod.GET)
	public String view(ModelMap modelMap) {
		log.info("testing the rest");
		Users users = restOperations.getForObject(
				"https://127.0.0.1:8443/demo/services/users", Users.class);

		modelMap.put("users", users.getUser());
		return "rest/rest";
	}

}
