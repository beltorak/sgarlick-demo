package com.home.web.config;

import java.util.Locale;

import javax.annotation.Resource;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.home.core.config.DatabaseConfig;
import com.home.core.config.SecurityConfig;
import com.home.core.i18n.JdbcMessageSource;
import com.home.service.config.ServiceConfig;

@Configuration
@EnableWebMvc
@Import({ DatabaseConfig.class, ServiceConfig.class, SecurityConfig.class })
@ComponentScan(basePackages = "com.home.web.controller")
public class MvcConfig extends WebMvcConfigurerAdapter {
	@Resource
	private NamedParameterJdbcOperations namedParameterJdbcOperations;

	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/jsp/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	@Bean
	public RestTemplate restTemplate() {

		DefaultHttpClient client = new DefaultHttpClient();
		BasicCredentialsProvider provider = new BasicCredentialsProvider();

		provider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(
				"admin", "admin"));
		client.setCredentialsProvider(provider);
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(
				client);

		return new RestTemplate(factory);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**")
				.addResourceLocations("/WEB-INF/static/")
				.setCachePeriod(31556926);
	}

	@Override
	public Validator getValidator() {
		try {
			return validator();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Bean
	public MessageSource messageSource() throws Exception {
		JdbcMessageSource messageSource = new JdbcMessageSource();
		messageSource
				.setNamedParameterJdbcOperations(namedParameterJdbcOperations);
		messageSource.setUseCodeAsDefaultMessage(true);
		messageSource
				.setSqlStatement("select text from ref_resource_bundle where text_id = ? and language = ?");
		messageSource.setCachedMilliSecond(1000000);
		messageSource.setDefaultLocale(Locale.ENGLISH);
		return messageSource;
	}

	@Profile("dev")
	@Configuration
	static class ShowExceptions {
		@Bean
		public HandlerExceptionResolver simpleMappingExceptionResolver() {
			SimpleMappingExceptionResolver resolver = new SimpleMappingExceptionResolver();
			resolver.setDefaultErrorView("error/default-error");
			resolver.setDefaultStatusCode(500);
			return resolver;
		}
	}

	@Profile("prod")
	@Configuration
	static class HideExceptions {
		@Bean
		public HandlerExceptionResolver simpleMappingExceptionResolver() {
			SimpleMappingExceptionResolver resolver = new SimpleMappingExceptionResolver();
			resolver.setDefaultErrorView("error/hidden-error");
			resolver.setDefaultStatusCode(500);
			return resolver;
		}
	}

	@Bean
	public LocalValidatorFactoryBean validator() throws Exception {
		LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
		bean.setValidationMessageSource(messageSource());
		return bean;
	}
}