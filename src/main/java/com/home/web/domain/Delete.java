package com.home.web.domain;

import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotEmpty;

@XmlRootElement
public class Delete {

	@NotEmpty
	private Collection<String> ids;

	public Collection<String> getIds() {
		return ids;
	}

	public void setIds(Collection<String> id) {
		this.ids = id;
	}
}
