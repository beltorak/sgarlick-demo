package com.home.web.domain.messeges;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class FieldValidationMessage {
	
	public FieldValidationMessage()
	{
		super();
		fieldMessage = new ArrayList<FieldMessage>();
	}
	public FieldValidationMessage(Collection<FieldMessage> fieldMessage,
			Message message) {
		super();
		this.fieldMessage = fieldMessage;
		this.message = new ArrayList<Message>();
		this.message.add(message);
	}
	
	public FieldValidationMessage(Collection<FieldMessage> fieldMessage,
			Collection<Message> message) {
		super();
		this.fieldMessage = fieldMessage;
		this.message = message;
	}
	public Collection<FieldMessage> getFieldMessage() {
		return fieldMessage;
	}
	
	public void setFieldMessage(Collection<FieldMessage> fieldMessage) {
		this.fieldMessage = fieldMessage;
	}

	private Collection<FieldMessage> fieldMessage;
	private Collection<Message> message;
	public Collection<Message> getMessage() {
		return message;
	}
	
	public void setMessage(Collection<Message> message) {
		this.message = message;
	}

}
