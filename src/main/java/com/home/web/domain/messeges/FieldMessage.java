package com.home.web.domain.messeges;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FieldMessage {
	
	private String field;
	private String message;
	public String getField() {
		return field;
	}
	
	public void setField(String field) {
		this.field = field;
	}
	public FieldMessage() {
		super();
	}
	public FieldMessage(String field, String message) {
		super();
		this.field = field;
		this.message = message;
	}
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
}
