package com.home.service;

import java.util.Collection;

import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.transaction.annotation.Transactional;

import com.home.core.domain.User;

public interface UserService {

	@Transactional
	@PreAuthorize("hasRole('CAP_DOIT')")
	public abstract Collection<String> put(Collection<User> users);

	@Transactional
	@PreAuthorize("hasRole('CAP_DOIT')")
	public abstract String put(User users);

	@Transactional
	@PreAuthorize("hasRole('CAP_DOIT')")
	@PreFilter("hasPermission(filterObject, 'isUserCreator') or hasRole('CAP_DELETE_ALL')")
	public abstract void delete(Collection<String> userIds);

	@Transactional
	@PreAuthorize("hasRole('CAP_DELETE_ALL') or hasPermission(#userId, 'isUserCreator')")
	public abstract void delete(String userId);

	@PostFilter("hasPermission(filterObject.userId,'isUserCreator') or hasRole('CAP_READ_ALL')")
	public abstract Collection<User> fetch();

	@PreAuthorize("hasRole('CAP_READ_ALL') or hasPermission(#userId, 'isUserCreator')")
	public abstract User fetch(String userId);
	
	public abstract Collection<User> fetchLike(String likeUserId);

}
