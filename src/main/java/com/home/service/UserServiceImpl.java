package com.home.service;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.home.core.dao.UserDAO;
import com.home.core.domain.User;
import com.home.core.security.SecurityHolder;

@Service
public class UserServiceImpl implements UserService {
	@Resource
	private UserDAO userDAO;

	@Resource
	private SecurityHolder securityHolder;

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Override
	public User fetch(String userId) {
		return userDAO.fetch(userId);
	}

	@Override
	public Collection<User> fetch() {
		return userDAO.fetch();
	}

	@Override
	public Collection<User> fetchLike(String likeUserId) {
		if (StringUtils.isEmpty(likeUserId))
			return fetch();
		return userDAO.fetchLike(StringUtils.replace(likeUserId, "*", "%"));
	}

	@Override
	public void delete(String userId) {
		userDAO.delete(userId);
	}

	@Override
	public void delete(Collection<String> userIds) {
		if (!CollectionUtils.isEmpty(userIds))
			userDAO.delete(userIds);
	}

	@Override
	public String put(User user) {
		if (userDAO.put(user, securityHolder.getName())) {
			return user.getUserId() + " created.";
		} else {
			return user.getUserId() + " updated.";
		}
	}

	@Override
	public Collection<String> put(Collection<User> users) {
		Collection<String> messages = new ArrayList<String>();
		for (User user : users) {
			String message = put(user);
			messages.add(message);
		}
		return messages;
	}
}
