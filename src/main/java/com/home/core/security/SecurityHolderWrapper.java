package com.home.core.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityHolderWrapper implements SecurityHolder {

	/* (non-Javadoc)
	 * @see com.home.core.security.SecurityHolder#getPrincipal()
	 */
	@Override
	public Object getPrincipal() {
		return SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
	}

	/* (non-Javadoc)
	 * @see com.home.core.security.SecurityHolder#getAuthentication()
	 */
	@Override
	public Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	/* (non-Javadoc)
	 * @see com.home.core.security.SecurityHolder#getName()
	 */
	@Override
	public String getName() {
		return SecurityContextHolder.getContext().getAuthentication().getName();
	}
}
