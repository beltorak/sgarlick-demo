package com.home.core.security;

import org.springframework.security.core.Authentication;

public interface SecurityHolder {

	public abstract Object getPrincipal();

	public abstract Authentication getAuthentication();

	public abstract String getName();

}