package com.home.core.security.permissions;

public class PermissionNotDefinedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PermissionNotDefinedException() {
		super();
	}

	public PermissionNotDefinedException(String message) {
		super(message);
	}
}
