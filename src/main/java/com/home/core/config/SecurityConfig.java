package com.home.core.config;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.DelegatingAuthenticationEntryPoint;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.util.RequestMatcher;

import com.home.core.security.AjaxRequestMatcher;
import com.home.core.security.SecurityHolder;
import com.home.core.security.SecurityHolderWrapper;
import com.home.core.security.permissions.DefaultPermissionEvaluatorImpl;
import com.home.core.security.permissions.Permission;
import com.home.core.security.permissions.UserCreatedByPermission;

@Configuration
@ImportResource(value = "classpath:/com/home/web/security/application-context-spring-security-common.xml")
public class SecurityConfig {

	@Resource
	private NamedParameterJdbcOperations namedParameterJdbcOperations;

	/**
	 * This bean simple wraps the static class {@link SecurityContextHolder} and
	 * provides access to security information in an injectable bean
	 * 
	 * @return
	 */
	@Bean
	public SecurityHolder securityHolder() {
		return new SecurityHolderWrapper();
	}

	/**
	 * This entry point will map AjaxRequest to a
	 * {@link Http403ForbiddenEntryPoint} to allow ajax request to receive a 403
	 * status code and other request will be defaulted to a
	 * {@link LoginUrlAuthenticationEntryPoint}
	 * 
	 * @return
	 */
	@Bean
	public DelegatingAuthenticationEntryPoint authEntryPoint() {
		LinkedHashMap<RequestMatcher, AuthenticationEntryPoint> entryPoints = new LinkedHashMap<RequestMatcher, AuthenticationEntryPoint>();
		entryPoints.put(new AjaxRequestMatcher(),
				new Http403ForbiddenEntryPoint());
		DelegatingAuthenticationEntryPoint authEntryPoint = new DelegatingAuthenticationEntryPoint(
				entryPoints);
		authEntryPoint
				.setDefaultEntryPoint(new LoginUrlAuthenticationEntryPoint(
						"/login"));
		return authEntryPoint;
	}

	@Bean
	public DefaultMethodSecurityExpressionHandler expressionHandler() {
		DefaultMethodSecurityExpressionHandler handler = new DefaultMethodSecurityExpressionHandler();
		handler.setPermissionEvaluator(permissionEvaluator());
		return handler;
	}

	@Bean
	public DefaultPermissionEvaluatorImpl permissionEvaluator() {
		Map<String, Permission> permissions = new HashMap<String, Permission>();
		permissions.put("isUserCreator", new UserCreatedByPermission(
				namedParameterJdbcOperations));
		return new DefaultPermissionEvaluatorImpl(permissions);
	}

	@Configuration
	@Profile("dev")
	@ImportResource(value = "classpath:/com/home/web/security/application-context-spring-security-dev.xml")
	static class DevSecurity {

	}

	@Configuration
	@Profile("prod")
	@ImportResource(value = "classpath:/com/home/web/security/application-context-spring-security.xml")
	static class ProdSecurity {

	}

}
