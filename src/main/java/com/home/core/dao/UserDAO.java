package com.home.core.dao;

import java.util.Collection;

import org.springframework.transaction.annotation.Transactional;

import com.home.core.domain.User;

public interface UserDAO {

	public abstract Collection<User> fetch();

	public abstract User fetch(String userId);

	@Transactional
	public abstract Boolean put(User user, String createdBy);

	@Transactional
	public abstract void delete(Collection<String> userIds);

	@Transactional
	public abstract void delete(String userId);

	public abstract Collection<User> fetchLike(String likeUserId);

}