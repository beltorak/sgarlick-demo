package com.home.core.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Repository;

import com.home.core.domain.User;
import com.home.core.exception.UserNotFoundException;

@Repository
public class UserDAOImpl implements UserDAO {
	private static final String SELECT_FROM_T_USERS = "select user_id, first_name, last_name, address, city, state, email, zip_code from t_users";

	@Resource
	private NamedParameterJdbcOperations namedParameterJdbcOperations;

	private RowMapper<User> userInfoMapper = new RowMapper<User>() {

		@Override
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User();
			user.setUserId(rs.getString(1));
			user.setFirstName(rs.getString(2));
			user.setLastName(rs.getString(3));
			user.setAddress(rs.getString(4));
			user.setCity(rs.getString(5));
			user.setState(rs.getString(6));
			user.setEmail(rs.getString(7));
			user.setZipCode(rs.getString(8));

			return user;
		}
	};

	@Override
	public Collection<User> fetch() {
		return namedParameterJdbcOperations.getJdbcOperations().query(
				SELECT_FROM_T_USERS, userInfoMapper);
	}

	@Override
	public Collection<User> fetchLike(String likeUserId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", likeUserId);
		return namedParameterJdbcOperations.query(SELECT_FROM_T_USERS
				+ " where user_id like :userId", params, userInfoMapper);
	}

	@Override
	public User fetch(String userId) {
		User user = null;
		try {
			Map<String, String> params = new HashMap<String, String>();
			params.put("userId", userId);
			user = namedParameterJdbcOperations.queryForObject(
					SELECT_FROM_T_USERS + " where user_id = :userId", params,
					userInfoMapper);
		} catch (EmptyResultDataAccessException e) {
			throw new UserNotFoundException();
		}
		return user;
	}

	@Override
	public Boolean put(User user, String createdBy) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("userId", user.getUserId());
		params.put("firstName", user.getFirstName());
		params.put("lastName", user.getLastName());
		params.put("address", user.getAddress());
		params.put("email", user.getEmail());
		params.put("city", user.getCity());
		params.put("state", user.getState());
		params.put("zipCode", user.getZipCode());
		params.put("createdBy", createdBy);
		if (0 == namedParameterJdbcOperations
				.update("update t_users set first_name = :firstName, last_name = :lastName, email = :email, address = :address, city = :city, state = :state, zip_code = :zipCode where user_id = :userId",
						params)) {

			namedParameterJdbcOperations
					.update("insert into t_users(user_id,  first_name, last_name, email, address, city, state, zip_code, created_by) values(:userId, :firstName, :lastName, :email, :address, :city, :state, :zipCode, :createdBy)",
							params);
			return true;
		}
		return false;

	}

	@Override
	public void delete(Collection<String> userIds) {
		Map<String, Collection<String>> params = new HashMap<String, Collection<String>>();
		params.put("userIds", userIds);
		namedParameterJdbcOperations.update(
				"delete from t_users where user_id in (:userIds)", params);
	}

	@Override
	public void delete(String userId) {
		delete(Arrays.asList(new String[] { userId }));
	}

}
