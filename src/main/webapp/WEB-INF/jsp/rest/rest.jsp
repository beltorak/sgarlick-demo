<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<!DOCTYPE html>
<html>
<head>
<title>View All Users via REST</title>
</head>
<body>
	<form id="user-form">
		<div id="info">
			<c:if test="${empty users}">No Users</c:if>
			<div id="field">
				The following users were fetched by restOperations.getForObject(
				"https://127.0.0.1/demo/rest/service/users", Users.class); using admin credentials.
				<c:forEach items="${users}" var="user">
					<div>${user.userId}
					</div>
				</c:forEach>
			</div>

		</div>
	</form>
</body>
</html>