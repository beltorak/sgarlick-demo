<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<title>Create or Update a User</title>
<script src="<c:url value="/resources/js/users/modify.js"/>" async defer></script>
</head>
<body>
	<form id="put-user-form">
		<div>
			<fieldset id="info">
				<legend>Create or Update User</legend>
				<div>
					<div id="field">
						<div class="field">
							<label for="userId">User Id:</label>
						</div>
						<div class="field">
							<label for="firstName">First Name:</label>
						</div>
						<div class="field">
							<label for="lastName">Last Name:</label>
						</div>
						<div class="field">
							<label for="address">Address:</label>
						</div>
						<div class="field">
							<label for="city">City:</label>
						</div>
						<div class="field">
							<label for="state">State:</label>
						</div>
						<div class="field">
							<label for="zipCode">ZIP Code:</label>
						</div>
						<div class="field">
							<label for="email">Email:</label>
						</div>
					</div>
					<div id="data">
						<div class="data">
							<input id="userId" type="text" name="userId"><span
								id="userId-message" class="field-error-message"></span>
						</div>
						<div class="data">
							<input id="firstName" name="firstName" type="text"><span
								id="firstName-message" class="field-error-message"></span>
						</div>
						<div class="data">
							<input id="lastName" name="lastName" type="text"><span
								id="lastName-message" class="field-error-message"></span>
						</div>
						<div class="data">
							<input id="address" name="address" type="text"><span
								id="address-message" class="field-error-message"></span>
						</div>
						<div class="data">
							<input id="city" name="city" type="text"><span
								id="city-message" class="field-error-message"></span>
						</div>
						<div class="data">
							<input id="state" name="state" type="text"><span
								id="state-message" class="field-error-message"></span>
						</div>
						<div class="data">

							<input id="zipCode" name="zipCode" type="text"><span
								id="zipCode-message" class="field-error-message"></span>
						</div>
						<div class="data">
							<input id="email" name="email" type="text"><span
								id="email-message" class="field-error-message"></span>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
		<div>
			<span id="all-message"></span>
		</div>
		<div>
			<input type="button" id="put-user-button-json"
				value="Put User send JSON return JSON">
		</div>
		<div class="output">
			<div class="demo-buttons">
				<div>Additional Demos</div>
				<div>
					<input type="button" id="put-user-button-xml"
						value="Put User send JSON return XML"> <input
						type="button" id="put-multi-user-button-json"
						value="Put Multi User send JSON return JSON"> <input
						type="button" id="put-multi-user-button-xml"
						value="Put Multi User send XML return XML">
				</div>
			</div>
			<div>Request:</div>
			<div id="request" class="data-output"></div>
			<div>Response:</div>
			<div id="response" class="data-output"></div>
		</div>
	</form>
</body>
</html>