
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
	$.mydata = { userId : '${userId}' };
</script>
<script src="<c:url value="/resources/js/users/user.js"/>" async defer></script>
</head>
<body>
	<div id="info">
		<div id="field">
			<div class="field">User ID:</div>
			<div class="field">Fist Name:</div>
			<div class="field">Last Name:</div>
			<div class="field">Address:</div>
			<div class="field">City:</div>
			<div class="field">State:</div>
			<div class="field">ZIP Code:</div>
			<div class="field">Email:</div>
		</div>
		<div id="data">
			<div class="data">${user.userId}</div>
			<div class="data">${user.firstName}</div>
			<div class="data">${user.lastName}</div>
			<div class="data">${user.address}</div>
			<div class="data">${user.city}</div>
			<div class="data">${user.state}</div>
			<div class="data">${user.zipCode}</div>
			<div class="data">${user.email}</div>
		</div>
	</div>
	<form>
		<div class="output">
			<div>
				<input type="button" value="Fetch JSON" id="get-user-json-button"><input
					type="button" value="Fetch XML" id="get-user-xml-button">
			</div>
			<div>
				<div>Response:</div>
				<div id="response" class="data-output"></div>
			</div>
		</div>
	</form>
</body>
</html>