<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="page" uri="http://www.opensymphony.com/sitemesh/page"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<!DOCTYPE html">
<html>
<head>
<title>Insert title here</title>
</head>
<body>
	<h1>
		<spring:message code="header" />
	</h1>
	<sec:authorize access="isAuthenticated()">
		<div>
			<a href="<c:url value="/logout" />">Logout</a>
		</div>
	</sec:authorize>
</body>
</html>