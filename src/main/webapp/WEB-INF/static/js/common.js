$.fn.serializeObject = function() {
	var o = {};
	$.each(this.serializeArray(), function() {
		if (o[this.name] !== undefined) {
			if (!o[this.name].push) {
				o[this.name] = [ o[this.name] ];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

$.ajaxSetup({
	cache : false,
	error : function(data) {
		if(data.status == 403)
			location.reload();
		else if(data.status == 500)
		{
			document.open();
			document.write(data.responseText);
			document.close();
		} else if(data.status == 404)
			{
				window.alert("The data you are trying to access does not exists or you do not have persmission to perform the operation.");
			}
	}
});
