	$(document).ready(function() {
	
		function response(dataType) {
			$.ajax({
				url : '/demo/users/',
				dataType : dataType,
				data :  $('#search-box').serialize(),
				type : 'GET'
			}).done(function(data) {
				$('#response').text(dataType == 'json' ? JSON.stringify(data, null, '\t') : (new XMLSerializer()).serializeToString(data));
			});
		}

		$('#get-xml-button').click(function(event){
			response('xml');
			event.preventDefault();
		});
		$('#get-json-button').click(function(event){
			response('json');
			event.preventDefault();
		});
		$('#user-form').on('submit', function(event){
			$('#user-form').load('/demo/users #user-form, script', $('#search-box').serialize());
			event.preventDefault();
		});

		$('#user-form').on('click', '#search-button', function(event) {
			$('#user-form').submit();
			event.preventDefault();
		});
		
		$('#user-form').on('change', '#search-box', function(event){
			$('#search-button').click();
			event.preventDefault();
		});

		
		$('#user-form').on('click','#delete-button', function(event){
			$.ajax({
				url : '/demo/users/',
				type : 'DELETE', 
				contentType : 'application/json',
				data : '{ "ids":["herp","derp"]}',
				dataType : 'json'
			}).done(function(){
				$('#user-form').load('/demo/users #user-form', $('#search-box').serialize());
			});

			event.preventDefault();
		});
		
		$('#user-form').on('click','#delete-fail-button', function(event){
			$.ajax({
				url : '/demo/users/',
				type : 'DELETE', 
				contentType : 'application/json',
				data : '{ "ids":[]}',
				dataType : 'json'
			}).fail(function(data){
				if(data.status = '400') {
					$('#error-response').text(JSON.stringify(JSON.parse(data.responseText), null, '\t'));
				}
			});

			event.preventDefault();
		});
		
		$('#user-form').on('click', '.delete', function(event) {
			$.ajax({
				url : '/demo/users/' + $(this).attr('id'),
				type : 'DELETE',
				dataType : 'json'
			}).done(function(){
					$('#user-form').load('/demo/users #user-form',$('#search-box').serialize());
				});

			event.preventDefault();
		});
		$('#search-button').click();
	});